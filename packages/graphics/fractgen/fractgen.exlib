# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=dreibh tag=${PNV} ] cmake
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="An extensible Fractal Generator software"

HOMEPAGE+=" https://www.uni-due.de/~be0001/fractalgenerator/"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="kde"

KF5_MIN_VER="5.2.0"
QT_MIN_VER="5.3.0"

DEPENDENCIES="
    build:
        x11-libs/qttools:5[>=${QT_MIN_VER}] [[ note = lrelease-qt5 ]]
    build+run:
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        kde? (
            kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
            kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
            kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        )
"

CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=( KDE )

fractgen_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

fractgen_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

