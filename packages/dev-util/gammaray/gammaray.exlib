# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="GammaRay"

require github [ user='KDAB' tag=v${PV} ] cmake
require gtk-icon-cache
require flag-o-matic

export_exlib_phases pkg_setup src_prepare src_compile src_install

SUMMARY="A tool for examining the internals of a Qt application at runtime"
DESCRIPTION="
GammaRay augments conventional debuggers by understanding the implementation
of Qt, allowing it to visualize application behavior on a higher level,
especially with complex frameworks like scene graphs, model/view or state
machines being involved."

HOMEPAGE="https://www.kdab.com/kdab-products/gammaray/"

LICENCES="
    BSD-2    [[ note = [ 3rdparty/lz4 ] ]]
    BSD-3    [[ note = [ 3rdparty/StackWalker, cmake scripts ] ]]
    GPL-2
    LGPL-2   [[ note = [ 3rdpart/kde ] ]]
    LGPL-2.1 [[ note = [ 3rdparty/qt ] ]]
"

SLOT="0"
MYOPTIONS="
    doc
    designer             [[ description = [ Allows exporting widget .ui files ] ]]
    geolocation          [[ description = [ Inspection tool for QtLocation geo positioning data ] ]]
    kjob                 [[ description = [ KJob tracker plugin ] ]]
    qtscript-debugger    [[ description = [ Builds a script debugger attachable to any QScriptEngine ] ]]
    state-machine-viewer [[ description = [ Builds a plugin which visualizes state machines ] ]]
    wayland              [[ description = [ QtWayland compositor inspector plug-in ] ]]
    web-inspector        [[ description = [ HTML/CSS/DOM/JS introspection on any QWebPage ] ]]
"

QT_MIN_VER="5.5.0"

DEPENDENCIES="
    build:
        dev-lang/perl:*   [[ note = [ pod2man ] ]]
        x11-libs/qttools:5
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5
        )
    build+run:
        dev-util/elfutils
        kde-frameworks/syntax-highlighting:5[>=5.28]
        x11-dri/mesa
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
        geolocation? ( x11-libs/qtlocation:5[>=${QT_MIN_VER}] )
        kjob? ( kde-frameworks/kcoreaddons:5 )
        qtscript-debugger? ( x11-libs/qtscript:5[>=${QT_MIN_VER}][tools(+)] )
        wayland? (
            sys-libs/wayland[>=1.12]
            x11-libs/qtwayland:5
        )
        web-inspector? ( x11-libs/qtwebengine:5[>=${QT_MIN_VER}] )
    test:
        sys-devel/gdb
"

# 16 of 21 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Unwritten, provide Next-gen graphical state machine viewer
    -DCMAKE_DISABLE_FIND_PACKAGE_KDSME:BOOL=TRUE
    # Unwritten: https://github.com/KhronosGroup/glslang
    -DCMAKE_DISABLE_FIND_PACKAGE_Glslang:BOOL=TRUE
    # Unwritten: Qt3D
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DAnimation:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DExtras:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DInput:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DLogic:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DRender:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt53DQuick:BOOL=TRUE
    # Unwritten: qtconnectivity
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Bluetooth:BOOL=TRUE
    # Unwritten: Qt IVI (In-Vehicle Infotainment)
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5IviCore:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5IviVehicleFunctions:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5IviMedia:BOOL=TRUE
    # Unwritten
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Scxml:BOOL=TRUE
    # Unwritten, would enable a "highly experimental" plugin, "just for toying arround"
    -DCMAKE_DISABLE_FIND_PACKAGE_VTK:BOOL=TRUE
    -DECM_MKSPECS_INSTALL_DIR:PATH="/usr/$(exhost --target)/lib/qt5/mkspecs"
    -DDATAROOTDIR:PATH=/usr/share
    -DQCH_INSTALL_DIR=/usr/share/doc/qt5
    -DGAMMARAY_BUILD_UI:BOOL=TRUE
    -DGAMMARAY_CLIENT_ONLY_BUILD:BOOL=FALSE
    # Defaults to false but I feel more comfortable with true for now, bundles
    # https://cgit.kde.org/kuserfeedback.git Also a bit silly because of the
    # negation, e.g. turns up in cmake's summary as enabled, when it's
    # actually disabled.
    -DGAMMARAY_DISABLE_FEEDBACK:BOOL=TRUE
    -DGAMMARAY_INSTALL_QT_LAYOUT:BOOL=TRUE
    # Used for reading debug info/backtraces. libdb from elfutils is first
    # searched for with ...AUTO_DETECT=TRUE, so we use that.
    -DSTACK_DETAILS_AUTO_DETECT:BOOL=FALSE
    -DSTACK_DETAILS_DW:BOOL=TRUE
    -DSTACK_DETAILS_BFD:BOLL=FALSE
    -DSTACK_DETAILS_BACKTRACE_SYMBOL:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'doc Doxygen'
    'designer Qt5Designer'
    'geolocation Qt5Location'
    'geolocation Qt5Positioning'
    'kjob KF5CoreAddons'
    'qtscript-debugger Qt5Script'
    'qtscript-debugger Qt5ScriptTools'
    Wayland
    'wayland Qt5WaylandCompositor'
    'web-inspector Qt5WebEngineWidgets'
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'doc GAMMARAY_BUILD_DOCS'
)

gammaray_pkg_setup() {
    # Fixes an build error with LibreSSL and ld.bfd, cf.
    # https://bugreports.qt.io/browse/QTBUG-63291
    append-ldflags -Wl,--no-fatal-warnings
}

gammaray_src_prepare() {
    cmake_src_prepare

    # Don't install docs into a wrong dir and let emagicdocs do the work
    edo sed -e '/install(FILES \${DOCS} DESTINATION/d' \
            -i CMakeLists.txt

    edo sed -e '/QHELPGEN_EXECUTABLE /s/qhelpgenerator/qhelpgenerator-qt5/' \
            -i docs/CMakeLists.txt
}

gammaray_src_compile() {
    default
    option doc && emake docs
}

gammaray_src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd docs/api
        dodoc -r html
        edo popd
    fi
}

