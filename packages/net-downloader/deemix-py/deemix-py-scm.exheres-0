# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# last checked commit: f530a4e89f930fb36db2ee2b9a4454bce825d3c9

SCM_REPOSITORY="https://git.freezer.life/RemixDev/deemix-py.git"
SCM_BRANCH="main"

require setup-py [ blacklist="2 3.6" import=setuptools ] scm-git

SUMMARY="A deezer downloader built from the ashes of Deezloader Remix"
DESCRIPTION="The base library (or core) can be used as a stand alone CLI app
or implemented in an UI using the API."

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/click[python_abis:*(-)?]
        dev-python/deezer-py[python_abis:*(-)?]
        dev-python/mutagen[python_abis:*(-)?]
        dev-python/pycryptodome[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/spotipy[>=2.11.0][python_abis:*(-)?]
"

# Doesn't seem to have any tests
RESTRICT="test"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Don't need pycryptodomex and pycrypto at the same time, so use it as a
    # drop-in replacement, cf. pycryptodome's README.rst
    edo sed -e "s/pycryptodomex/pycryptodome/" -i setup.py -i requirements.txt
    edo sed -e "s/from Cryptodome./from Crypto./" \
        -i deemix/utils/crypto.py
}

