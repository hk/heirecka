# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=Cura

require github [ user=Ultimaker ] cmake
require python [ blacklist=2 multibuild=false ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="3D printer / slicing GUI built on top of the Uranium framework"
DESCRIPTION="
Ultimaker Cura is a state-of-the-art slicer application to prepare your 3D
models for printing with a 3D printer. With hundreds of settings and hundreds
of community-managed print profiles, Ultimaker Cura is sure to lead your next
project to a success."

HOMEPAGE+=" https://ultimaker.com/software/ultimaker-cura"

LICENCES="LGPL-3.0"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        3d-printing/curaengine
        3d-printing/fdm-materials
        dev-libs/libcharon[python_abis:*(-)?]
        dev-libs/libsavitar[python_abis:*(-)?]
        dev-python/certifi[python_abis:*(-)?]
        dev-python/keyring[python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/pynest2d[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/shapely[python_abis:*(-)?]
        dev-python/uranium[python_abis:*(-)?]
        net-libs/libarcus[python_abis:*(-)?]
    test:
        dev-python/pytest[python_abis:*(-)?]
    run:
        x11-libs/qtgraphicaleffects:5
        x11-libs/qtquickcontrols:5
        x11-libs/qtquickcontrols2:5
    suggestion:
        dev-python/pyserial[python_abis:*(-)?] [[
            description = [ Required for USB printing ]
        ]]
        dev-python/zeroconf[python_abis:*(-)?] [[
            description = [ Required to detect mDNS-enabled printers ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCURA_BINARY_DATA_DIRECTORY:PATH="/usr/share/cura/"
    -DCURA_BUILDTYPE=Exheres
    # Both would need https://github.com/mikedh/trimesh/
    -DCURA_NO_INSTALL_PLUGINS="AMFReader;TrimeshReader"
    -DCURA_VERSION=${PV}
    -DGETTEXT_MSGINIT_EXECUTABLE="msginit"
    -DPython3_EXECUTABLE=${PYTHON}
)

cura_src_test() {
    # code-style is a bit like -Werror, also saves us a mypy dep
    edo ctest -E 'code-style' --verbose
}

cura_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

cura_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

