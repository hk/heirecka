# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require udev-rules [ udev_files=[ etc/udev/rules.d/99-flashforge.rules ] ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="Slicer for the FlashForge 3D printers"

HOMEPAGE="https://www.flashforge.com/product-detail/40"
DOWNLOADS="http://www.ishare3d.com/3dapp/public/FlashPrint-ii/FlashPrint/${PN}_${PV}_amd64.deb"

LICENCES="proprietary"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        x11-dri/glu
        x11-dri/mesa
        x11-libs/qtbase:5
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

WORK=${WORKBASE}

src_unpack() {
    default

    unpack ./data.tar.xz
}

src_prepare() {
    default
    edo sed -e "s|^Exec=.*|Exec=FlashPrint|" \
        -i usr/share/applications/FlashPrint.desktop
}

src_install() {
    # When installing the FlashPrint exectuable to /usr/$(exhost --target)/bin
    # slicing doesn't work, so don't change the unsual location and install a
    # symlink.
    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/bin
    dosym /usr/share/FlashPrint/FlashPrint /usr/$(exhost --target)/bin/FlashPrint
    exeinto usr/share/FlashPrint/
    doexe usr/share/FlashPrint/FlashPrint
    exeinto usr/share/FlashPrint/engine
    # Needs to be executable for slicing to work
    doexe usr/share/FlashPrint/engine/ffslicer.exe
    edo rm usr/share/FlashPrint/{FlashPrint,engine/ffslicer.exe}

    # Install the rest
    insinto /usr/share
    doins -r usr/share/*

    # Fix docdir
    edo mv "${IMAGE}"/usr/share/doc/flashprint{,-${PVR}}

    install_udev_files
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

